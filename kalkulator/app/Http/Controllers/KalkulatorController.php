<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{
    public function  index(){
        return view('kalkulator');
    }

    public function hasil(Request $request)
    {
        switch ($request->operator) {
            case "+":
                $jawab = $request->angka1 + $request->angka2;
                break;
            case "-":
                $jawab = $request->angka1 - $request->angka2;
                break;
            case "x":
                $jawab = $request->angka1 * $request->angka2;
                break;
            case "/":
                if ($request->angka2 != 0) {
                    $jawab = $request->angka1 / $request->angka2;
                    break;
                } else {
                    $jawab = "tidak bisa dilakukan";
                    break;
                }
            default: $jawab = "error"; break;
        }

        $hasil = [
            "angka1" => $request->angka1,
            "angka2" => $request->angka2,
            "operator" => $request->operator,
            "jawab" => $jawab
        ];

        return view('kalkulator', ['hasil' => $hasil]);
    }
}
