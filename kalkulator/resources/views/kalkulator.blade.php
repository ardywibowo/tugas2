<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kalkulator</title>
</head>
<body>
<h2>Kalkulator Sederhana</h2>
<form method="get" action="/kalkulator/hasil">
    <label>Masukkan angka pertama</label><br/>
    <input type="number" name="angka1" required><br/>
    <label>Masukkan angka kedua</label><br/>
    <input type="number" name="angka2" required><br/><br/>
    <input type="submit" value="+" name="operator">
    <input type="submit" value="-" name="operator">
    <input type="submit" value="x" name="operator">
    <input type="submit" value="/" name="operator">
</form>
@if (isset( $hasil ))
    <h2>{{$hasil['angka1']}} {{$hasil['operator']}} {{$hasil['angka2']}} = {{$hasil['jawab']}}</h2>
@endif
</body>
</html>
