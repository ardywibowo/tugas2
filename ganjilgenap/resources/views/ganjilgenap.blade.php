<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cetak Ganjil Genap</title>
</head>
<body>
<form method="get" action="/ganjilgenap/hasil">
    <label>Masukkan angka pertama</label><br/>
    <input type="number" name="angka1" required><br/>
    <label>Masukkan angka kedua</label><br/>
    <input type="number" name="angka2" required><br/>
    <input type="submit" value="masukkan"><br/><br/>
</form>

@if (isset( $hasil ))
    <?php $i=1?>
    <table border="1" aria-describedby="hasil">
        <thead>
        <tr>
            <th id="number">No</th>
            <th id="hasil">Hasil</th>
        </tr>
        </thead>
        <tbody>
        @foreach($hasil as $h)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $h }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</body>
</html>
