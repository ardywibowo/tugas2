<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    public function index() {
        return view('ganjilgenap');
    }
    public function hasil(Request $request)
    {
        if($request->angka2<$request->angka1)
        {
            $temp = $request->angka1;
            $request->angka1 = $request->angka2;
            $request->angka2 = $temp;
        }

        for ($request->angka1; $request->angka1 <= $request->angka2; $request->angka1++) {
            if ($request->angka1 % 2 == 0) {
                $hasil[] = "Angka " . $request->angka1 . " adalah genap";
            } else {
                $hasil[] = "Angka " . $request->angka1 . " adalah ganjil";
            }
        }

        return view('ganjilgenap', ['hasil' => $hasil]);
    }
}
