<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HitungVokalController extends Controller
{
    public function  index(){
        return view('hitungvokal');
    }

    public function  hasil(Request $kata){

        $text = htmlspecialchars($kata->text);
        $array = str_split($text);
        $vocal = ['a', 'i', 'u', 'e', 'o'];

        $hurufvokal = array_intersect($vocal, $array);
        $jumlah = count($hurufvokal);

        foreach ($hurufvokal as $h)
        {
            $huruf[] = $h;
        }

        if($jumlah > 0)
        {
            $hasil = $text." = " .$jumlah. " yaitu ";
            for ($i=0;$i<$jumlah;$i++)
            {
                if($i>0){$hasil = $hasil . " dan ";}
                $hasil = $hasil.$huruf[$i];
            }
        }
        else
        {
            $hasil = $text." = tidak ada huruf vokal";
        }
        return view('hitungvokal', ['hasil' => $hasil]);
    }
}
